# COA - Challenge

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.0.0.

## 1 - Install dependencies

Run command: `npm install`

## 2 - Development server

Run `ng serve` for a dev server.
Navigate to `http://localhost:4200/`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.
