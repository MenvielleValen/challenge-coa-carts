import { Component, OnInit } from '@angular/core';
import {FormGroup} from '@angular/forms';
import {FormlyFieldConfig} from '@ngx-formly/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { validateEmail } from 'src/app/utils/validateEmail';
import {MatSnackBar} from '@angular/material/snack-bar';


//Models y Services
import { FirebaseService } from '../../services/firebase.service';
import { User } from '../../models/user.model';
import { CartService } from '../../services/cart.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  loading:boolean=false;
  form: FormGroup = new FormGroup({});
  model: User = {displayName:'', email:'', emailRepeat:'', password:''};
  formError: any ={};
  errorForm:boolean = false;


  constructor(private router: Router, private auth: AngularFireAuth ,private _snackBar: MatSnackBar, 
    private _firebaseService :FirebaseService, private _cartService: CartService) 
  {}
  
  //Init
  ngOnInit(): void {
    this.auth.authState.subscribe((data)=>{
      if(data){
        this.router.navigate(['/home'])
      }
    })
  }

  // Generacion de campos del formulario de registro
  fields: FormlyFieldConfig[] = [
    {
      key: 'displayName',
      type: 'input',
      className: 'field-form',
      templateOptions: {
        label: 'Ingrese su nombre',
        placeholder: 'Ingrese su nombre completo',
        required: true,   
       
      },
    },
    {
      key: 'email',
      type: 'input',
      className: 'field-form',
      templateOptions: {
        label: 'Ingrese su email',
        placeholder: 'Ingrese su email',
        required: true,   
       
      },
    },
    {
      key: 'emailRepeat',
      type: 'input',
      className: 'field-form',
      templateOptions: {
        label: 'Confirme su email',
        placeholder: 'Confirme su email',
        required: true,
            
      },
    },
    {
      key: 'password',
      type: 'input',
      className: 'field-form',
      templateOptions: {
        label: 'Ingrese su contraseña',
        placeholder: 'Ingrese su contraseña',
        required: true,
        type:'password'

      }
      
    }
  ];

  //Al hacer click en registrarse se ejecuta, si los campos son válidos crea el usuario. 
  onSubmit() {
    if(this.validateFields()){
      this.createUser();
    }
    else{
      this.openSnackBar("Revisa los errores", "Cerrar");
    }
    
  }

  //Valida los campos del formulario (Si es email y longitud de contraseña)
  validateFields():boolean{
    if(this.model.email != this.model.emailRepeat){
      this.formError = {
        error: 'Los emails no coinciden.'
      }
      this.errorForm = true;
      return false;
    }
    if(!validateEmail(this.model.email)){
      this.formError = {
        error: 'Ingrese un email válido.'
      }
      this.errorForm = true;
      return false;
    }
    if(this.model.password.trim().length < 6){
      this.formError = {
        error: 'La contraseña debe contener como mínimo 6 caracteres.'
      }
      this.errorForm = true;
      return false;
    }
    else{
      this.errorForm = false;
      return true;
    }

  }
  
  //Creacion del usuario y del carrito correspondiente
  createUser(){
    this.loading = true;
    this._firebaseService.createUser(this.model)
      .then((res)=>{
            this.loading = false;
            this.openSnackBar("Registrado correctamente", "Cerrar");       
            this._cartService.authUser()
            .then((res)=>{
              this._cartService.loadCart();
              this.router.navigate(['/home'])
            });
      })
      .catch((err)=>{
          this.loading = false;
          this.openSnackBar(err.message, "Cerrar");
          console.log(err.message)
      });
  }

  //abrir SnackBar 
  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 3000,
      horizontalPosition: 'right',
    });
  }
}
