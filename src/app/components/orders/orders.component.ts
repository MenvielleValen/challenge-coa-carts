import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CartService } from '../../services/cart.service';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})
export class OrdersComponent implements OnInit {

  orders: any[] = [];

  constructor(private _cartService: CartService, private router: Router) { }
  //Init
  ngOnInit(): void {
    this._cartService.authUser()
    .then((res)=>{
      if(res){
        this._cartService.loadOrders()
        .then((res:any)=>{
           res.subscribe((data:any)=>{
             data.docs.map((element:any)=>{
               this.orders.push(element.data());
             })
           })
        })
        
      }
      else{
        this.router.navigate(['/home'])
      }
    })
  }



}
