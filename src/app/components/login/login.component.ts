import { Component, OnInit } from '@angular/core';
import {FormGroup} from '@angular/forms';
import { Router } from '@angular/router';
import {FormlyFieldConfig} from '@ngx-formly/core';
import { FirebaseService } from '../../services/firebase.service';
import { User } from '../../models/user.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  form: FormGroup = new FormGroup({});
  model: User = {displayName: '', email: '' , emailRepeat:'', password: ''};
  errorLog:string = '';
  loading:boolean=false;

  constructor(private _firebaseService: FirebaseService, private router: Router,) {   
  }
  
  ngOnInit(): void { 
    this._firebaseService.userAuth().subscribe((data)=>{
      if(data){
        this.router.navigate(['/home'])
      }
    })

  }
  // Generacion de campos del formulario de login
  fields: FormlyFieldConfig[] = [
    {
      key: 'email',
      type: 'input',
      className: 'field-form',
      templateOptions: {
        label: 'Email',
        placeholder: 'Ingrese su email',
        required: true,   
      },
    },
    {
      key: 'password',
      type: 'input',
      className: 'field-form',
      templateOptions: {
        label: 'Contraseña',
        placeholder: 'Ingrese su contraseña',
        required: true,
        type:'password'
      }
    }
  ];
  
  //Valida y envia formulario
  onSubmit() {
    this.loading = true;
    if(this.model.email == "" || this.model.password == ""){
      this.errorLog = "Completa los campos";
      this.loading = false;
      return;
    }
    this._firebaseService.userLogin(this.model)
    .then(()=>{
      this.loading = false;
    })
    .catch(()=>{
      this.errorLog = "Usuario y/o contraseña incorrectos";
      this.loading = false;
    })

  }

}
