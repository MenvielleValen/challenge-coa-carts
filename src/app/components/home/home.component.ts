import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { FirebaseService } from '../../services/firebase.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  userLogged: any;
  isUserLogged:boolean = false;

  constructor(private _firebaseService: FirebaseService, private router: Router) {  }

  ngOnInit(): void {
    this.userAuth();
  }

  //Checkea si el usuario esta autenticado
  userAuth():void{
    this._firebaseService.userAuth().subscribe((user)=>{
      if(user){
        this.isUserLogged = true;
        this.userLogged = user;
        this.router.navigate(['/home']);
      }
      else{
        this.isUserLogged = false;
        this.router.navigate(['/home']);       
      }
    })

  }

}
