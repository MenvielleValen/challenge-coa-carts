import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FirebaseService } from '../../services/firebase.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { CartService } from 'src/app/services/cart.service';


@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  openCart: boolean=false;
  count:any = 0;
  userLogged:boolean=false;

 
  constructor(private router: Router, private _firebaseService: FirebaseService, 
  private _snackBar: MatSnackBar) {
    
  }

  //Init
  ngOnInit(): void {
    this._firebaseService.userAuth().subscribe((user)=>{
        if(user){
          this.userLogged = true;
          
        }
        else{
          this.userLogged = false;
        }
    })
   
  }

  //Abre el carrito
  public onCart():void{
    this.openCart = !this.openCart;
  }

  //Cierrra la sesion y deslogea al usuario
  userLogOut():void{
    this._firebaseService.userLogOut().then(()=>{
      this.openSnackBar("Ha cerrado sesión correctamente", "Cerrar")
      this.router.navigate(['/home'])
      window.location.reload();
    })
    .catch((err)=>{
      console.log(err.message)
      this.openSnackBar("Ha ocurrido un error!", "Cerrar")
    });
  }

  //Abre snackbar
  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 3000,
      horizontalPosition: 'right',
      
    }); 
  } 

}
