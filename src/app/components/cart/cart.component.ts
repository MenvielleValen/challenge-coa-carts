import { Component, OnInit} from '@angular/core';
import { NavbarComponent } from '../navbar/navbar.component';
import { CartService } from '../../services/cart.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ProductInCart } from 'src/app/models/product-cart.model';




@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css'],
  providers:[CartService]
})
export class CartComponent implements OnInit{
  
  products: any[] = [];
  productsInCart: ProductInCart[] = [];
  countProducts: number = 0;
  priceTotal: number = 0;
  loading: boolean = false;
  btnOptions: boolean = false;
  
  
  constructor(private navBar: NavbarComponent, private cartService: CartService, private _snackBar: MatSnackBar) { }

  //Init 
  ngOnInit(): void {
    this.cartService.authUser().then(()=>{
      this.getProducts();
    })
  }

  //Obtiene los productos que estan en el carrito activo (status:pending)
  getProducts(){
    this.restart();
    this.cartService.getProductsInCart()
    .then((res:any)=>{  
       res.subscribe((data:any)=>{
          data.docs.map((element:any)=>{
            this.productsInCart.push(element.data())
          })
          this.getProductsById();
       })
    })
    .catch((err:any)=>{
      console.log(err.message)
      this.openSnackBar("Ha ocurrido un error", "Cerrar");
    });
  }

  //Reinicia variables
  restart(){
    this.productsInCart = []
    this.products = []
    this.priceTotal = 0;
  }

  //Obtiene los productos originales utilizando como referencia el id de los que estan en el carrito
  getProductsById(){
    this.productsInCart.map((element)=>{
      this.cartService.getProductsById(element.product_id)
      .then((res:any)=>{
         res.subscribe((data:any)=>{
            this.products.push({productId:element.id ,quantity: element.quantity, ...data.data()})
            this.priceTotal+=(parseInt(data.data().price) * element.quantity);
         })
      })
    })
  }

  //Añade un producto más al carrito
  addOne(id: string){
    this.cartService.addOneProduct(id)
    .then(()=>{
      this.getProducts();
    })
    .catch((err:any)=>{
      console.log(err.message)
      this.openSnackBar("Ha ocurrido un error!", "Cerrar")
    })

  }

  //Elimina un producto del carrito
  removeOne(id: string){
    this.cartService.removeOneProduct(id)
    .then(()=>{
      this.getProducts();
    })
    .catch((err:any)=>{
      console.log(err.message)
      this.openSnackBar("Ha ocurrido un error!", "Cerrar")
    })
  }
  
  //Elimina el prodcuto del carrito (Completo, no importa cantidad)
  deleteProduct(id: string){
    this.cartService.deleteProduct(id)
    .then(()=>{
      this.getProducts();
      this.openSnackBar("Producto eliminado", "Cerrar")
    ;})
    .catch((err)=>{
      console.log(err.message)
    });
  }

  //Abre menu de confirmación
  openUserOptions(){this.btnOptions = true}

  //Crea la orden 
  createOrder(){
    if(this.products.length > 0){
      this.cartService.createOrder(this.priceTotal);
      this.openSnackBar("Orden creada correctamente", "Cerrar")
      this.closeMenu();
    }
    else{
      this.openSnackBar("Su carrito se encuentra vacio!", "Cerrar")
    }
  }

  //Cancela la orden
  cancelOrder(){
    this.btnOptions = false;
  }

  //Cierra el carrito
  closeMenu():void{
    this.navBar.onCart();
  }

  //abrir SnackBar 
  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 3000,
      horizontalPosition: 'right'
    });
  }

}
