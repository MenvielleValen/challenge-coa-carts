import { Component, OnInit} from '@angular/core';

//Models y servicios
import { CartService } from '../../services/cart.service';
import { ProductModel } from '../../models/product.model';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { FirebaseService } from 'src/app/services/firebase.service';




@Component({
  selector: 'app-products-list',
  templateUrl: './products-list.component.html',
  styleUrls: ['./products-list.component.css']
})
export class ProductsListComponent implements OnInit {
  
  
  products: ProductModel[] = [];
  userLogged:boolean = false;
  isOpenViewItem: boolean = false;
  viewItem:ProductModel = {id:"",name:"",description:"",img:"",price:0};
  loading:boolean = false; 


  constructor(private _firebaseService: FirebaseService, private _cartService:CartService, private _snackBar: MatSnackBar, private router: Router) {
    this._firebaseService.userAuth().subscribe((res)=>{
      if(res){
        this.userLogged = true;

      }
      else{
        this.userLogged = false;;
      }
    })
  }

  //Init
  ngOnInit(): void {
    this.loading = true;

    this.getProducts();
  }

  //Obtiene todos los productos
  getProducts(){
    this._cartService.getProducts().subscribe((res)=>{
      this.products = res.map((e:any)=>{
          return{
            id : e.payload.doc.data().id,
            name :  e.payload.doc.data().name,
            description :  e.payload.doc.data().description,
            price :  e.payload.doc.data().price,
            img: e.payload.doc.data().img,
          }
      })
      this.loading = false;
    })
  }

  //Añade un producto al carrito
  //=> product = modelo completo del producto
  addProduct(product: ProductModel){
    this._cartService.addProduct(product)
    .then(()=>{
      this.openSnackBar("Producto añadido al carrito", "Cerrar");
    })
    .catch((err)=>{
      this.openSnackBar("Ha ocurrido un error", "Cerrar");
      console.log(err.message)
    });
  }

  //Abre la ventana de detalles de producto
  openViewItem(item: any){
    this.viewItem = item;
    this.isOpenViewItem = true;
  }

  goToRegister(){
    this.router.navigate(['/register'])
  }

  //Cierra ventana de detalles de producto
  closeViewItem(){
    this.isOpenViewItem = false;
  }

  //Abre snackbar
   openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 3000,
      horizontalPosition: 'right'
    });

  }

 

  

}
