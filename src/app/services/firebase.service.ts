import { Injectable } from "@angular/core";
import { AngularFireAuth } from "@angular/fire/auth";
import { Observable } from "rxjs";
import { User } from "../models/user.model";
import { Router } from '@angular/router';



@Injectable({
    providedIn:'root'
})
export class FirebaseService{

    constructor(private afAuth: AngularFireAuth, private router: Router){}

    //Crea un nuevo usuario
    createUser(user: User): Promise<any>{
        return this.afAuth.createUserWithEmailAndPassword(user.email, user.password)
        .then((res)=>{
            if(res){
               res.user?.updateProfile({
                 displayName: user.displayName
               })
    
            }
        });
    }

    //Login de usuario
    userLogin(user:User):Promise<any>{
        return this.afAuth.signInWithEmailAndPassword(user.email, user.password);
    }

   //Devuelve estado de autenticacion
    userAuth(): Observable<any>{
        return this.afAuth.authState
    }

    //Cierra sesión
    userLogOut(){
       
        return this.afAuth.signOut();
    }


}