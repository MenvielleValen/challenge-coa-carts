import { Injectable} from "@angular/core";
import { AngularFirestore } from "@angular/fire/firestore";
import { AngularFireAuth } from "@angular/fire/auth";
import { ProductModel } from '../models/product.model';
import { Router } from '@angular/router';
import { Observable } from "rxjs";


@Injectable({
    providedIn:'root'
})
export class CartService{

    path = "carts/"
    uid:string="";
    cart:any;

    constructor(private auth: AngularFireAuth, private db: AngularFirestore, private router:Router){}

    //Autentica al usuario y chequea si tiene un carrito pendiente, si es asi lo abre, de lo contrario le crea uno.
    async authUser(){
        return new Promise(async(resolve,reject)=>{
            try{
                this.auth.authState.subscribe((user:any)=>{
                    if(user){
                        this.uid = user.uid;
                        this.loadCart()
                        .then((res:any)=>{
                            res.subscribe((data:any)=>{
                                data.docs.map((element:any)=>{
                                    this.cart = element.data();
                                    resolve(element);
                                })
                                if(!this.cart){
                                    this.createCart()
                                    .then((res)=>{
                                        resolve(res);
                                    });
                                }
                            })
                      
                        });                 
                    }
                    else{
                        this.uid = "";
                        resolve(null);
                    }
                });
            }
            catch(err:any){
                console.log(err.message);
            }
        })
    }
    
    //Carga el carrito pendiente si es que existe
    async loadCart(){
        return new Promise(async(resolve, reject)=>{
            try{
                const path = 'users/'+this.uid+"/"+this.path;
                const result = this.db.collection(path, ref=> ref.where("status","==","pending")).get();
                resolve(result);
            }
            catch(err:any){
                console.log(err.message)
            }
        })
  
    }

    //Carga el carrito completos si es que existe
    async loadOrders(){
        return new Promise(async(resolve, reject)=>{
            try{
                const path = 'users/'+this.uid+"/"+this.path;
                const result = this.db.collection(path, ref=> ref.where("status","==","completed")).get();
                resolve(result);
            }
            catch(err:any){
                console.log(err.message)
            }
        })
  
    }

    //Crea un carrito nuevo 
    async createCart(){
        return new Promise((resolve, reject)=>{
            try{
                const path = 'users/'+this.uid+"/carts"
                const id = this.db.createId();
                const result = this.db.collection(path).doc(id).set({
                    id,
                    uid: this.uid,
                    status: 'pending'
                })
                this.cart = {
                    id,
                    uid: this.uid,
                    status: 'pending'
                }
                resolve(result);
            }
            catch(err:any){
                console.log(err.message)
            }
        })
    }

    //Obtiene todos los productos
    getProducts(){
        return this.db.collection('products').snapshotChanges();
    }

    //Obtiene todos los productos almacenados en el carrito
    async getProductsInCart(){
        return new Promise(async(resolve,reject)=>{
            try{
                const path = 'users/'+this.uid+"/product_carts";
                const result = this.db.collection(path, ref => ref.where("cart_id","==",this.cart.id)).get();
                resolve(result)
            }
            catch(err:any){
                console.log(this.cart)
                console.log(err.message)
            }
        })

    }

    //Obtiene los productos originales por ID
    async getProductsById(id: string){
        return new Promise(async(resolve, reject)=>{
            try{
                const path = 'products';
                const result = this.db.collection(path).doc(id).get();
                resolve(result);
            }
            catch(err:any){
                console.log(err.message)
            }
        })
    }

    //Elimina el producto correspondiente
    // => ID : id del producto a eliminar
    async deleteProduct(id: any){
        return new Promise(async(resolve,reject)=>{
            try{
                const path = 'users/'+this.uid+"/product_carts";
                const result = this.db.collection(path).doc(id).delete();
                resolve(result)
            }
            catch(err:any){
                console.log(err.message);
            }   
        })
    }

    //Añade un producto nuevo, antes de eso comprueba si ya existe un producto similar en el carrito
    //de ser asi solo aumenta su cantidad
    // => product : modelo del producto
    async addProduct(product: ProductModel){
        return new Promise(async(resolve, reject)=>{
            try{
                this.authUser().then((res)=>{
                        if(!res){this.router.navigate(['/products'])}
                        const path = 'users/'+this.uid+"/product_carts";
                        this.db.collection(path, ref => ref.where("product_id", "==", product.id).where("cart_id","==",this.cart.id)).get()
                        .subscribe((data:any)=>{
                            if(data.docs.length <= 0){
                                const id:any = this.db.createId();
                                const data = {id, 
                                    product_id: product.id,
                                    cart_id: this.cart.id,
                                    quantity:1,
                                }
                                const result = this.db.collection(path).doc(id).set(data);
                                resolve(result);
                             }
                             else{
                                const id = data.docs[0].id;
                                const quantity = data.docs[0].data().quantity + 1;
                                const result = this.db.collection(path).doc(id).update({
                                    quantity: quantity
                                })
                                resolve(result);
                             }
                        })
                }
                ).catch((err:any)=>{
                    console.log(err.message)
                })
            }
            catch(err:any){
                reject(err.message)
            }
        })
    }

    //Quita un producto en base a su cantidad, si queda en 0 automaticamente lo elimina del carrito
    // => id: id del producto a remover
    removeOneProduct(id:string){
        return new Promise(async(resolve, reject)=>{
            try{
                const path = 'users/'+this.uid+"/product_carts";
                this.db.collection(path).doc(id).get().subscribe((data:any)=>{

                    if(data.data().quantity <= 1){
                       resolve(this.deleteProduct(id));
                    }
                    else{
                        const result =  this.db.collection(path).doc(id).update({
                            quantity: data.data().quantity - 1
                        })
                        resolve(result)
                    }
                    
                }); 
               
            } 
            catch(err:any){
                console.log(err.message)
            }
        })
    }

    //Añada un producto desde el carrito, suma 1 en cantidad
      // => id: id del producto a agregar
    async addOneProduct(id:string){
        return new Promise(async(resolve, reject)=>{
            try{
                const path = 'users/'+this.uid+"/product_carts";
                this.db.collection(path).doc(id).get().subscribe((data:any)=>{
                    const result =  this.db.collection(path).doc(id).update({
                        quantity: data.data().quantity + 1
                    })
                    resolve(result)
                }); 
               
            } 
            catch(err:any){
                console.log(err.message)
            }
        })
    }

    //Crea la orden y cambia el status del carrito a completado
    async createOrder(price:number = 0){
        return new Promise(async(resolve)=>{
            try{
                this.authUser().then((res)=>{
                    const path = 'users/'+this.uid+"/"+this.path;
                    const date = new Date().getDate();
                    const month = new Date().getMonth();
                    const year = new Date().getFullYear();

                    const nowDate = `${date} / ${month} / ${year}`;

                    const result = this.db.collection(path, ref=> ref.where("status","==","pending")).doc(this.cart.id).update({
                        status:"completed",
                        date: nowDate,
                        finalPrice: price
                    })
                    this.createCart();
                    resolve(result);
                })  
            }
            catch(err:any){
                console.log(err.message)
            }
        })
    }


}